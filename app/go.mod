module zerodha.tech/devops-task

go 1.13

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/gomodule/redigo v1.8.9
	github.com/knadh/koanf v0.6.1
)
